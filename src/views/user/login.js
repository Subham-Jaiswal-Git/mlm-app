import React from 'react';
import { Row, Card, CardTitle, Label, FormGroup, Button } from 'reactstrap';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';

import { Formik, Form, Field } from 'formik';

import { loginUser } from 'redux/actions';
import { Colxx } from 'components/common/CustomBootstrap';
import IntlMessages from 'helpers/IntlMessages';

const validatePassword = (value) => {
  let error;
  if (!value) {
    error = 'Please enter your password';
  } else if (value.length < 8) {
    error = 'Value must be longer than 7 characters';
  }
  return error;
};

const validateUsername = (value) => {
  let error;
  if (!value) {
    error = 'Please enter your username';
  }
  return error;
};

const validateTwoFa = (value) => {
  let error;
  if (!value) {
    error = 'Please enter your 2FA';
  }
  return error;
};

const Login = ({ history, loading, twoFactor, loginUserAction }) => {
  const onUserLogin = (values) => {
    if (!loading) {
      if (values.email !== '' && values.password !== '') {
        loginUserAction(values, history);
      }
    }
  };

  return (
    <Row className="h-100">
      <Colxx xxs="12" md="10" className="mx-auto my-auto">
        <Card className="auth-card">
          <div className="position-relative image-side ">
            <p className="text-white h2">MAGIC IS IN THE DETAILS</p>
            <p className="white mb-0">
              Please use your credentials to login.
              <br />
              If you are not a member, please{' '}
              <NavLink to="/user/register" className="white">
                register
              </NavLink>
              .
            </p>
          </div>
          <div className="form-side">
            <NavLink to="/" className="white">
              <span className="logo-single" />
            </NavLink>
            <CardTitle className="mb-4">
              <IntlMessages id="user.login-title" />
            </CardTitle>
            <Formik
              initialValues={{
                username: '',
                password: '',
                two_factor_code: '',
              }}
              onSubmit={onUserLogin}
            >
              {({ errors, touched }) => (
                <Form className="av-tooltip tooltip-label-bottom">
                  <FormGroup className="form-group has-float-label">
                    <Label>
                      <IntlMessages id="user.username" />
                    </Label>
                    <Field
                      className="form-control"
                      name="username"
                      validate={validateUsername}
                    />
                    {errors.username && touched.username && (
                      <div className="invalid-feedback d-block">
                        {errors.username}
                      </div>
                    )}
                  </FormGroup>
                  <FormGroup className="form-group has-float-label">
                    <Label>
                      <IntlMessages id="user.password" />
                    </Label>
                    <Field
                      className="form-control"
                      type="password"
                      name="password"
                      validate={validatePassword}
                    />
                    {errors.password && touched.password && (
                      <div className="invalid-feedback d-block">
                        {errors.password}
                      </div>
                    )}
                  </FormGroup>
                  {twoFactor && (
                    <FormGroup className="form-group has-float-label">
                      <Label>
                        <IntlMessages id="user.two-factor-code" />
                      </Label>
                      <Field
                        className="form-control"
                        name="two_factor_code"
                        validate={validateTwoFa}
                      />
                      {errors.two_factor_code && touched.two_factor_code && (
                        <div className="invalid-feedback d-block">
                          {errors.two_factor_code}
                        </div>
                      )}
                    </FormGroup>
                  )}
                  <div className="d-flex mb-4 justify-content-between align-items-center">
                    <NavLink to="/user/forgot-password">
                      <IntlMessages id="user.forgot-password-question" />
                    </NavLink>
                    <Button
                      color="primary"
                      className={`btn-shadow btn-multiple-state ${
                        loading ? 'show-spinner' : ''
                      }`}
                      type="submit"
                      size="lg"
                    >
                      <span className="spinner d-inline-block">
                        <span className="bounce1" />
                        <span className="bounce2" />
                        <span className="bounce3" />
                      </span>
                      <span className="label">
                        <IntlMessages id="user.login-button" />
                      </span>
                    </Button>
                  </div>
                  <div className="d-flex justify-content-center align-items-center">
                    <span className="label">
                      <IntlMessages id="user.new-user-question" />
                      <NavLink to="/user/register">
                        <IntlMessages id="user.register-button" />
                      </NavLink>
                    </span>
                  </div>
                </Form>
              )}
            </Formik>
          </div>
        </Card>
      </Colxx>
    </Row>
  );
};
const mapStateToProps = ({ authUser }) => {
  const { loading, twoFactor } = authUser;
  return { loading, twoFactor };
};

export default connect(mapStateToProps, {
  loginUserAction: loginUser,
})(Login);
