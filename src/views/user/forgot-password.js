import React from 'react';
import { Row, Card, CardTitle, Label, FormGroup, Button } from 'reactstrap';
import { NavLink } from 'react-router-dom';
import { toast } from 'react-toastify';
import { Formik, Form, Field } from 'formik';
import { connect } from 'react-redux';
import { Colxx } from 'components/common/CustomBootstrap';
import IntlMessages from 'helpers/IntlMessages';
import { resetPassword, sendOtpUser } from 'redux/actions';

const validateUsername = (value) => {
  let error;
  if (!value) {
    error = 'Please enter your username';
  }
  return error;
};

const validatePassword = (value) => {
  let error;
  if (!value) {
    error = 'Please enter your password';
  } else if (value.length < 8) {
    error = 'Value must be longer than 7 characters';
  }
  return error;
};

const validateOtp = (value) => {
  let error;
  if (!value) {
    error = 'Please enter otp';
  }
  return error;
};

const ForgotPassword = ({
  history,
  loading,
  otpSendForgot,
  resetPasswordAction,
  sendOtpUserAction,
}) => {

  const onResetPassword = (values) => {
    if (!loading) {
      if (values.username !== '' && values.newpassword !== '' && values.otp !== '') {
        resetPasswordAction(values, history);
      }
    }
  };

  const onUserSendOtp = (values) => {    
    if (!loading) {
      if (values.username !== '') {
        const params = values;
        params.is_otp_send_forgot = true;
        sendOtpUserAction(params, history);
      }else{
        toast.error('Please enter username!!');
      }
    }
  };  

  return (
    <Row className="h-100">
      <Colxx xxs="12" md="10" className="mx-auto my-auto">
        <Card className="auth-card">
          <div className="position-relative image-side ">
            <p className="text-white h2">MAGIC IS IN THE DETAILS</p>
            <p className="white mb-0">
              Please use your e-mail to reset your password. <br />
              If you are not a member, please{' '}
              <NavLink to="/user/register" className="white">
                register
              </NavLink>
              .
            </p>
          </div>
          <div className="form-side">
            <NavLink to="/" className="white">
              <span className="logo-single" />
            </NavLink>
            <CardTitle className="mb-4">
              <IntlMessages id="user.forgot-password" />
            </CardTitle>

            <Formik initialValues={{
              username:'',
              newpassword:'',
              otp:'',
            }} onSubmit={onResetPassword}>
              {({ values, errors, touched }) => (
                <Form className="av-tooltip tooltip-label-bottom">
                  <FormGroup className="form-group has-float-label">
                    <Label>
                      <IntlMessages id="user.username" />
                    </Label>
                    <Field
                      className="form-control"
                      name="username"
                      validate={validateUsername}
                    />
                    {errors.username && touched.username && (
                      <div className="invalid-feedback d-block">
                        {errors.username}
                      </div>
                    )}
                  </FormGroup>
                  <FormGroup className="form-group has-float-label">
                    <Label>
                      <IntlMessages id="user.new-password" />
                    </Label>
                    <Field
                      className="form-control"
                      type="password"
                      name="newpassword"
                      validate={validatePassword}
                    />
                    {errors.newpassword && touched.newpassword && (
                      <div className="invalid-feedback d-block">
                        {errors.newpassword}
                      </div>
                    )}
                  </FormGroup>
                  <FormGroup className="form-group has-float-label">
                    <Label>
                      <IntlMessages id="user.otp" />
                    </Label>
                    <Field
                      className="form-control"
                      name="otp"
                      validate={validateOtp}
                    />
                    {errors.otp && touched.otp && (
                      <div className="invalid-feedback d-block">
                        {errors.otp}
                      </div>
                    )}
                  </FormGroup>
                  <div className="d-flex justify-content-end align-items-end">
                    {otpSendForgot ? (                      
                      <Button
                        color="primary"
                        className={`btn-shadow btn-multiple-state ${
                          loading ? 'show-spinner' : ''
                        }`}
                        type="submit"
                        size="lg"
                      >
                        <span className="spinner d-inline-block">
                          <span className="bounce1" />
                          <span className="bounce2" />
                          <span className="bounce3" />
                        </span>
                        <span className="label">
                          <IntlMessages id="user.reset-password-button" />
                        </span>
                      </Button>
                    ):(
                      <Button
                        color="primary"
                        className={`btn-shadow btn-multiple-state ${
                          loading ? 'show-spinner' : ''
                        }`}
                        type="button"
                        size="lg"
                        onClick={()=>onUserSendOtp(values)}
                      >
                        <span className="spinner d-inline-block">
                          <span className="bounce1" />
                          <span className="bounce2" />
                          <span className="bounce3" />
                        </span>
                        <span className="label">
                          <IntlMessages id="user.send-otp-button" />
                        </span>
                      </Button>
                    )}                    
                  </div>
                </Form>
              )}
            </Formik>
          </div>
        </Card>
      </Colxx>
    </Row>
  );
};

const mapStateToProps = ({ authUser }) => {
  const { loading, otpSendForgot } = authUser;
  return { loading, otpSendForgot };
};

export default connect(mapStateToProps, {
  resetPasswordAction: resetPassword,
  sendOtpUserAction: sendOtpUser,
})(ForgotPassword);
