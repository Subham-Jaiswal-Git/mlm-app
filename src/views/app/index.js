import React, { Suspense } from 'react';
import { Route, withRouter, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import AppLayout from 'layout/AppLayout';
// import { ProtectedRoute, UserRole } from 'helpers/authHelper';

const Main = React.lazy(() =>
  import(/* webpackChunkName: "viwes-dashboard" */ './dashboard')
);
const SecondMenu = React.lazy(() =>
  import(/* webpackChunkName: "viwes-second-menu" */ './second-menu')
);

const WalletMenu = React.lazy(() =>
  import(/* webpackChunkName: "viwes-second-menu" */ './wallet')
);

const BlankPage = React.lazy(() =>
  import(/* webpackChunkName: "viwes-blank-page" */ './blank-page')
);

const ChangePassword = React.lazy(() =>
  import(/* webpackChunkName: "viwes-change-password" */ './change-password')
);

const Profile = React.lazy(() =>
  import(/* webpackChunkName: "viwes-profile-page" */ './profile')
);

const Kyc = React.lazy(() =>
  import(/* webpackChunkName: "viwes-profile-page" */ './kyc')
);

const App = ({ match }) => {
  return (
    <AppLayout>
      <div className="dashboard-wrapper">
        <Suspense fallback={<div className="loading" />}>
          <Switch>
            <Redirect exact from={`${match.url}/`} to={`${match.url}/dashboard`} />

            <Route
              path={`${match.url}/dashboard`}
              render={(props) => <Main {...props} />}
            />
            <Route
              path={`${match.url}/second-menu`}
              render={(props) => <SecondMenu {...props} />}
            />
            <Route
              path={`${match.url}/Wallet`}
              render={(props) => <WalletMenu {...props} />}
            />

            <Route
              path={`${match.url}/blank-page`}
              render={(props) => <BlankPage {...props} />}
            />

            <Route
              path={`${match.url}/kyc`}
              render={(props) => <Kyc {...props} />}
            />
            <Route
              path={`${match.url}/change-password`}
              render={(props) => <ChangePassword {...props} />}
            />

            <Route
              path={`${match.url}/profile`}
              render={(props) => <Profile {...props} />}
            />


            <Redirect to="/error" />
          </Switch>
        </Suspense>
      </div>
    </AppLayout>
  );
};

const mapStateToProps = ({ menu }) => {
  const { containerClassnames } = menu;
  return { containerClassnames };
};

export default withRouter(connect(mapStateToProps, {})(App));
