import React, { Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

const DepositHistory = React.lazy(() =>
  import(/* webpackChunkName: "second" */ './depositHistory')
);
const BinanceList = React.lazy(() =>
  import(/* webpackChunkName: "second" */ './binanceList')
);
const TicketsList = React.lazy(() =>
  import(/* webpackChunkName: "second" */ './tickets')
);
const WalletMenu = ({ match }) => (
  <Suspense fallback={<div className="loading" />}>
    <Switch>
      <Redirect exact from={`${match.url}/`} to={`${match.url}/wallet`} />
      <Route
        path={`${match.url}/deposit-history`}
        render={(props) => <DepositHistory {...props} />}
      />
       <Route
        path={`${match.url}/binance-list`}
        render={(props) => <BinanceList {...props} />}
      />
       <Route
        path={`${match.url}/tickets-list`}
        render={(props) => <TicketsList {...props} />}
      />
      <Redirect to="/error" />
    </Switch>
  </Suspense>
);
export default WalletMenu;
