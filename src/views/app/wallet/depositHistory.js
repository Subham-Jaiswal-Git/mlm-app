import React, { useEffect, useState } from 'react';
import { Row } from 'reactstrap';
import { Colxx, Separator } from 'components/common/CustomBootstrap';
import Breadcrumb from 'containers/navs/Breadcrumb';
import DataTable from 'react-data-table-component';
// import DataTable, { ExpanderComponentProps } from 'react-data-table-component';
import { DepositHistoryApi } from 'service/binance_wallet';
import { CSVLink } from "react-csv";
// import {BsPlusCircle} from "react-icons/bs";

const ExpandedComponent = ({ data }) =>

  <div className="container-fluid">
    <div className="mt-3 ml-3 mr-3 mb-3">
      <div>
        <div className='d-flex col-12 '>
          <div className="list-group-item col-6"><b>Address Tag :</b> {data.addressTag}</div>
          <div className="list-group-item col-6"><b>Insert Time :</b> {data.insertTime}</div>
        </div>

        <div className='d-flex col-12'>
          <div className="list-group-item col-6"><b>Transfer Type :</b> {data.transferType}</div>
          <div className="list-group-item col-6"><b>Confirm Times :</b> {data.confirmTimes}</div>
        </div>
        <div className='d-flex col-12 '>
          <div className="list-group-item col-6"><b>Unlock Confirm :</b> {data.unlockConfirm}</div>
          <div className="list-group-item col-6"><b>Wallet Type :</b> {data.walletType}</div>
        </div>
        <div className='d-flex  col-12'>
          <div className="list-group-item col-6"><b>Asset :</b> {data.asset}</div>
        </div>
      </div>
    </div>
  </div>;

const columns = [
  {
    name: <h6>Id</h6>,
    selector: row => row.id,
    sortable: true,
    width: "200px",
  },
  {
    name: <h6>Amount</h6>,
    selector: row => row.amount,
    sortable: true,
    width: "100px",
  },
  {
    name: <h6>Coin</h6>,
    selector: row => row.coin,
    sortable: true,
    width: "100px",
  },
  {
    name: <h6>Network</h6>,
    selector: row => row.network,
    sortable: true,
    width: "100px",
  },
  {
    name: <h6>Address</h6>,
    selector: row => row.address,
    sortable: true,
    width: "300px",
  },
  {
    name: <h6>Transaction Id</h6>,
    selector: row => row.txId,
    sortable: true,
    width: "240px",
  },
  {
    name: <h6>Status</h6>,
    selector: row => (<div> {row.status === 1 ? (<span >Active</span>) : (<span>Inactive</span>)} </div>),
    sortable: true,
    width: "100px",
  },
  // {
  //   name: <h6>Address Tag</h6>,
  //   selector: row => row.addressTag,
  //   sortable: true,
  //   // width: "140px",
  //   // style: {
  //   //   background: "orange",
  //   // },
  // },
  // {
  //   name: <h6>Insert Time</h6>,
  //   selector: row => row.insertTime,
  //   sortable: true,
  //   width: "fit-content",

  // },
  // {
  //   name: <h6>Transfer Type</h6>,
  //   selector: row => row.transferType,
  //   sortable: true,
  // },
  // {
  //   name: <h6>Confirm Times</h6>,
  //   selector: row => row.confirmTimes,
  //   sortable: true,
  // },
  // {
  //   name: <h6>Unlock Confirm</h6>,
  //   selector: row => row.unlockConfirm,
  //   sortable: true,
  // },
  // {
  //   name: <h6>Wallet Type</h6>,
  //   selector: row => row.walletType,
  //   sortable: true,
  // },
  // {
  //   name: <h6>Asset</h6>,
  //   selector: row => row.asset,
  //   sortable: true,
  // },

];

const DepositHistory = ({ match }) => {
  const [item, setItem] = useState([]);

  const getdata = async () => {
    const response = await DepositHistoryApi();
    console.log(response);
    setItem(response.data);
  }

  useEffect(() => {
    getdata();
    console.log(item)
  }, []);

  const tableCustomStyles = {
    headCells: {
      style: {
        marginTop: "10px",

      },
    },
  }

  // const isExpanded = row => row.defaultExpanded;
  // const PreExpanded = () => {
  //   const data = tableDataItems;
  //   data[0].defaultExpanded = true;
  // }
  // function customExpandIcon(props) {
  //   if (props.expanded) {
  //       return <Button title="Close the row" type="primary" htmltype="submit" icon="close" onClick={e => {
  //           props.onExpand(props.record, e);
  //       }}>Close</Button>
  //   } else {
  //           return <Button title="Click to open" htmltype="submit" icon="history" >Show
  //               History</Button>
  //   }
  // }

  return (
    <>
      <Row>
        <Colxx xxs="12">
          <Breadcrumb heading="menu.depositHistory" match={match} />
          <CSVLink data={item} filename="Deposit_History.csv">
            <button type="button" className="btn btn-md float-right btn-primary">Export</button>
          </CSVLink>
          <Separator className="mb-5" />
        </Colxx>
      </Row>

      <Row>
        <Colxx xxs="12" className="mb-4">
          <DataTable
            columns={columns}
            data={item}
            // selectableRows
            pagination
            fixedHeader
            fixedHeaderScrollHeight="600px"
            selectableRowsHighlight
            highlightOnHover
            customStyles={tableCustomStyles}
            expandableRows
            expandableRowsComponent={ExpandedComponent}
          // expandableRowExpanded={isExpanded}
          // expandIcon={(props) => this.customExpandIcon(props)}
          // customStyles={customStyles}
          // dense
          // actions={
          //   <CSVLink data={item} filename="Deposit_History.csv">
          //     <button type="button" className="btn btn-md mr-2 btn-primary mt-3">Export</button>
          //   </CSVLink>
          // }

          />
        </Colxx>
      </Row>
    </>)
};
export default DepositHistory;