import React from 'react';
import IntlMessages from 'helpers/IntlMessages';
import { Colxx, Separator } from 'components/common/CustomBootstrap';
import Breadcrumb from 'containers/navs/Breadcrumb';
import { adminRoot } from 'constants/defaultValues';
import { NavLink } from 'react-router-dom';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
// import moment from 'moment';
import { connect } from 'react-redux';
import { updateUser } from 'redux/actions'; 
import { Row, Card, CardBody, FormGroup, Label, Button, CardSubtitle, CardImg, CardText } from 'reactstrap';
import { FormikDatePicker } from '../../containers/form-validations/FormikFields';
import "../../assets/css/vendor/bootstrap.rtl.only.min.css";


const SignupSchema = Yup.object().shape({
  name: Yup.string().required('Name is required!'),
  email: Yup.string().email('Invalid email address').required('Email is required!'),
  phone: Yup.string().required('Mobile Number is required!').min(10, 'Enter a valid Mobile Number'),
  gender: Yup.string().required('A select option is required !'),
  address_line1: Yup.string().required('Address is required !'),
  address_line_2: Yup.string().required('Address 2 is required !'),
  landmark: Yup.string().required('Landmark is required !'),
  state: Yup.string().required('State is required !'),
  city: Yup.string().required('City is required !'),
  country: Yup.string().required('Country is required !'),
  pincode: Yup.string().required('Pincode is required !'),
  tax_id: Yup.string().required('Tax_Id is required !'),
  dob: Yup.string().required('Date required'),
});


const Profile = ({ match, history, loading, ProfileAction }) => {

  const onSubmit = (values) => {
    console.log(values);

    if (!loading) {
      if (
        values.name !== '' &&
        values.email !== '' &&
        values.phone !== '' &&
        values.gender !== '' &&
        values.country !== '' &&
        values.address_line1 !== '' &&
        values.address_line_2 !== '' &&
        values.landmark !== '' &&
        values.city !== '' &&
        values.state !== '' &&
        values.pincode !== '' &&
        values.dob !== '' &&
        values.tax_id !== ''
      ) {
        console.log(values)
        ProfileAction(values, history);
       
      }
    }
  };

  // console.log(values)
  return (
    <>
      <Row>
        <Colxx xxs="12">
          <Breadcrumb heading="menu.profile" match={match} />
          <Separator className="mb-5" />
        </Colxx>
      </Row>
      
      <Row className="mb-4 d-flex">
        <Colxx xxs="3">
          <Card className="mb-4">
            <CardBody>
              <div className="text-center">
                <CardImg
                  top
                  src="/assets/img/profiles/l-1.jpg"
                  alt="Card image cap"
                  className="img-thumbnail border-0 rounded-circle mb-4 list-thumbnail"
                />
                <NavLink to={`${adminRoot}/cards`}>
                  <CardSubtitle className="mb-1">Sarah Kortney</CardSubtitle>
                </NavLink>
                <CardText className="text-muted text-small mb-4">
                  Executive Director
                </CardText>
                <Button outline size="sm" color="primary">
                  Edit
                </Button>
              </div>
            </CardBody>
          </Card>
        </Colxx>

        <Colxx xxs="9">
          <Card>
            <CardBody>
              <h6 className="mb-4">Profile</h6>
              <Formik
                initialValues={{
                  name: 'Jenny James',
                  email: 'test@test.com',
                  phone: "9876543210",
                  gender: '0',
                  country: '0',
                  address_line1: "MB-407",
                  address_line_2: "Salt Lake Sector 5",
                  landmark: "Technopolis",
                  city: "Kolkata",
                  state: "West Bengal",
                  pincode: "700102",
                  dob: new Date(),
                  tax_id: ""
                }}
                validationSchema={SignupSchema}
                onSubmit={onSubmit}
              >
                {({
                  // handleSubmit,
                  setFieldValue,
                  setFieldTouched,
                  handleChange,
                  handleBlur,
                  values,
                  errors,
                  touched,
                  // isSubmitting,
                }) => (
                  <Form className="av-tooltip tooltip-label-right ">

                    <div className="d-flex ">
                      <FormGroup className="error-l-100 w-100 mr-2">
                        <Label>
                          <IntlMessages id="profile.fullname" />
                        </Label>
                        <Field className="form-control w-100 custom-control" name="name" value={values.name} />
                        {errors.name && touched.name ? (
                          <div className="invalid-feedback d-block">
                            {errors.name}
                          </div>
                        ) : null}
                      </FormGroup>

                      <FormGroup className="error-l-100 w-100 ml-2">
                        <Label>
                          <IntlMessages id="user.email" />
                        </Label>
                        <Field className="form-control" name="email" value={values.email} />
                        {errors.email && touched.email ? (
                          <div className="invalid-feedback d-block">
                            {errors.email}
                          </div>
                        ) : null}
                      </FormGroup>
                    </div>

                    <div className="d-flex">
                      <FormGroup className="error-l-100 w-100 mr-2">
                        <Label>
                          <IntlMessages id="profile.Mobile" />
                        </Label>
                        <Field className="form-control" name="phone" value={values.phone} />
                        {errors.phone && touched.phone ? (
                          <div className="invalid-feedback d-block">
                            {errors.phone}
                          </div>
                        ) : null}
                      </FormGroup>

                      <FormGroup className="error-l-100 w-100 ml-2">
                        <Label>
                          <IntlMessages id="profile.Gender" />
                        </Label>
                        <select
                          name="gender"
                          className="form-control"
                          value={values.gender}
                          onChange={handleChange}
                          onBlur={handleBlur}
                        >
                          <option selected>Select Gender</option>
                          <option value="male">Male</option>
                          <option value="female">Female</option>
                        </select>

                        {errors.gender && touched.gender ? (
                          <div className="invalid-feedback d-block">
                            {errors.gender}
                          </div>
                        ) : null}
                      </FormGroup>
                    </div>

                    <div className="d-flex ">
                      <FormGroup className="error-l-100 w-100 mr-2">
                        <Label>
                          <IntlMessages id="profile.Address_Line_1" />
                        </Label>
                        <Field className="form-control" name="address_line1" value={values.address_line1} />
                        {errors.address_line1 && touched.address_line1 ? (
                          <div className="invalid-feedback d-block">
                            {errors.address_line1}
                          </div>
                        ) : null}
                      </FormGroup>

                      <FormGroup className="error-l-100 w-100 ml-2">
                        <Label>
                          <IntlMessages id="profile.Address_Line_2" />
                        </Label>
                        <Field className="form-control" name="address_line_2" value={values.address_line_2} />
                        {errors.address_line_2 && touched.address_line_2 ? (
                          <div className="invalid-feedback d-block">
                            {errors.address_line_2}
                          </div>
                        ) : null}
                      </FormGroup>
                    </div>

                    <div className="d-flex ">
                      <FormGroup className="error-l-100 w-100 mr-2">
                        <Label>
                          <IntlMessages id="profile.Landmark" />
                        </Label>
                        <Field className="form-control" name="landmark" value={values.landmark} />
                        {errors.landmark && touched.landmark ? (
                          <div className="invalid-feedback d-block">
                            {errors.landmark}
                          </div>
                        ) : null}
                      </FormGroup>

                      <FormGroup className="error-l-100 w-100 ml-2">
                        <Label>
                          <IntlMessages id="profile.City" />
                        </Label>
                        <Field className="form-control" name="city" value={values.city} />
                        {errors.city && touched.city ? (
                          <div className="invalid-feedback d-block">
                            {errors.city}
                          </div>
                        ) : null}
                      </FormGroup>
                    </div>

                    <div className="d-flex">
                      <FormGroup className="error-l-100  w-100 mr-2">
                        <Label>
                          <IntlMessages id="profile.State" />
                        </Label>
                        <Field className="form-control" name="state" value={values.state} />
                        {errors.state && touched.state ? (
                          <div className="invalid-feedback d-block">
                            {errors.state}
                          </div>
                        ) : null}
                      </FormGroup>

                      <FormGroup className="error-l-100  w-100 ml-2">
                        <Label> <IntlMessages id="profile.Country" /></Label>
                        <select
                          name="country"
                          className="form-control"
                          value={values.country}
                          onChange={handleChange}
                          onBlur={handleBlur}
                        >
                          <option selected>Select Country</option>
                          <option value="India">India</option>
                          <option value="USA">USA</option>
                        </select>

                        {errors.country && touched.country ? (
                          <div className="invalid-feedback d-block">
                            {errors.country}
                          </div>
                        ) : null}
                      </FormGroup>
                    </div>

                    <div className="d-flex ">
                      <FormGroup className="error-l-100 w-100 mr-2">
                        <Label>
                          <IntlMessages id="profile.Pincode" />
                        </Label>
                        <Field className="form-control" name="pincode" value={values.pincode} />
                        {errors.pincode && touched.pincode ? (
                          <div className="invalid-feedback d-block">
                            {errors.pincode}
                          </div>
                        ) : null}
                      </FormGroup>

                      <FormGroup className="error-l-100 w-100 ml-2">
                        <Label className="d-block">
                          <IntlMessages id="profile.DOB" />
                        </Label>

                        <FormikDatePicker
                          name="dob"
                          // value={values.dob}
                          // let dob = {moment.utc(values.dob).format('MM-dd-yyyy  hh:mm:ss')}
                          // value= {values.dob === moment.utc(values.dob).format('YYYY-MM-DD HH:mm')}
                          // value={(moment(values.dob).format('yyyy-mm-dd hh:mm:ss'))}
                          onChange={setFieldValue}
                          onBlur={setFieldTouched}
                        />

                        { errors.dob && touched.dob ? (
                          <div className="invalid-feedback d-block">
                            {errors.dob}
                          </div>
                        ) : null }

                      </FormGroup>

                    </div>

                    <div className="d-flex ">
                      <FormGroup className="error-l-100 w-100">
                        <Label>
                          <IntlMessages id="profile.Tax_ID" />
                        </Label>
                        <Field className="form-control" name="tax_id" value={values.tax_id} />
                        {errors.tax_id && touched.tax_id ? (
                          <div className="invalid-feedback d-block">
                            {errors.tax_id}
                          </div>
                        ) : null}
                      </FormGroup>


                    </div>

                    <Button
                      color="primary"
                      className={`btn-shadow btn-multiple-state ${
                        loading ? 'show-spinner' : ''
                      }`}
                      size="lg"
                    >
                      <span className="spinner d-inline-block">
                        <span className="bounce1" />
                        <span className="bounce2" />
                        <span className="bounce3" />
                      </span>
                      <span className="label">
                        <IntlMessages id="user.profile" />
                      </span>
                    </Button>

                  </Form>
                )}
              </Formik>
            </CardBody>
          </Card>
        </Colxx>
      </Row>
    </>
  );
};

const mapStateToProps = ({ authUser }) => {
  const { loading } = authUser;
  return { loading };
};

export default connect(mapStateToProps, {
  ProfileAction : updateUser
})(Profile);
