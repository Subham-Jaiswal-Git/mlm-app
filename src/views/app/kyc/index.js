import React, { Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

const KycUpload = React.lazy(() =>
  import(/* webpackChunkName: "second" */ './kyc-upload')
);
const KycList = React.lazy(() =>
  import(/* webpackChunkName: "second" */ './kyc-list')
);

const KycMenu = ({ match }) => (
  <Suspense fallback={<div className="loading" />}>
    <Switch>
      <Redirect exact from={`${match.url}/`} to={`${match.url}/kyc`} />
      <Route
        path={`${match.url}/kyc-upload`}
        render={(props) => <KycUpload {...props} />}
      />
       <Route
        path={`${match.url}/kyc-list`}
        render={(props) => <KycList {...props} />}
      />
      <Redirect to="/error" />
    </Switch>
  </Suspense>
);
export default KycMenu;
