import React from 'react';
import IntlMessages from 'helpers/IntlMessages';
import { Colxx, Separator } from 'components/common/CustomBootstrap';
import Breadcrumb from 'containers/navs/Breadcrumb';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import { connect } from 'react-redux';
import { Row, Card, CardBody, FormGroup, Label, Button } from 'reactstrap';
import "../../assets/css/vendor/bootstrap.rtl.only.min.css";
import { changePassword } from 'redux/actions';

const SignupSchema = Yup.object().shape({
  currentpassword: Yup.string().required('Current Password is required!'),
  newpassword: Yup.string().required('New Password is required!'),
});

const ChangePassword = ({match, loading,changePasswordAction}) => {

  const onSubmit = (values) => {
    console.log(values);

    if (!loading) {
      if (
        values.currentpassword !== '' &&
        values.newpassword !== ''
      ) {
        changePasswordAction(values);
       }
    }
  };

  return (
    <>
      <Row>
        <Colxx xxs="12">
          <Breadcrumb heading="menu.change-password" match={match} />
          <Separator className="mb-5" />
        </Colxx>
      </Row>
    
      <Row>
        <Colxx xxs="6" className="mb-4">
        <Card>
            <CardBody>
              <h6 className="mb-4">Change Password</h6>
              <Formik
                initialValues={{
                  currentpassword: "",
                  newpassword: "",
                }}
                validationSchema={SignupSchema}
                onSubmit={onSubmit}
              >
                {({
                  values,
                  errors,
                  touched,
                  
                }) => (
                  <Form className="av-tooltip tooltip-label-right ">
                  
                      <FormGroup className="error-l-100 w-100 mr-2">

                        <Label>
                          <IntlMessages id="password.currentpassword" />
                        </Label>
                        <Field className="form-control w-100 custom-control" name="currentpassword" value={values.currentpassword} />
                        {errors.currentpassword && touched.currentpassword ? (
                          <div className="invalid-feedback d-block">
                            {errors.currentpassword}
                          </div>
                        ) : null}

                      </FormGroup>

                      <FormGroup className="error-l-100 w-100 ">

                        <Label>
                          <IntlMessages id="password.newpassword" />
                        </Label>
                        <Field className="form-control" name="newpassword" value={values.newpassword} />
                        {errors.newpassword && touched.newpassword ? (
                          <div className="invalid-feedback d-block">
                            {errors.email}
                          </div>
                        ) : null}

                      </FormGroup>
                      <Button
                      color="primary"
                      className={`btn-shadow btn-multiple-state ${
                        loading ? 'show-spinner' : ''
                      }`}
                      size="lg"
                    >
                      <span className="spinner d-inline-block">
                        <span className="bounce1" />
                        <span className="bounce2" />
                        <span className="bounce3" />
                      </span>
                      <span className="label">
                        <IntlMessages id="user.reset-password-button" />
                      </span>
                    </Button>

                    

                  </Form>
                )}
              </Formik>
            </CardBody>
          </Card>
        </Colxx>
      </Row>

    </>
  );
};

const mapStateToProps = ({ authUser }) => {
  const { loading , error} = authUser;
  return { loading, error };
};

export default connect(mapStateToProps, {
  changePasswordAction: changePassword
})(ChangePassword);
