import { setCurrentLanguage } from 'helpers/Utils';
import { CHANGE_LOCALE, CHANGE_LAYOUT } from '../contants';

// eslint-disable-next-line import/prefer-default-export
export const changeLocale = (locale) => {
  setCurrentLanguage(locale);
  return {
    type: CHANGE_LOCALE,
    payload: locale,
  };
};

export const changeLayout = (layout) => {
  setCurrentLanguage(layout);
  return {
    type: CHANGE_LAYOUT,
    payload: layout,
  };
};