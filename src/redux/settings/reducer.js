import { getCurrentLanguage } from 'helpers/Utils';
import { CHANGE_LOCALE, CHANGE_LAYOUT} from '../contants';

const INIT_STATE = {
  locale: getCurrentLanguage(),
  layout: 'layout1',
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case CHANGE_LOCALE:
      return { ...state, locale: action.payload };

    case CHANGE_LAYOUT:
      return { ...state, layout: action.payload };

    default:
      return { ...state };
  }
};
