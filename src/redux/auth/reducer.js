import { getCurrentUser, getCurrentUserProfile } from 'helpers/Utils';
import { isAuthGuardActive, currentUser } from 'constants/defaultValues';
import {
  LOGIN_USER,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_ERROR,
  LOGIN_USER_PROFILE,
  REGISTER_USER,
  REGISTER_USER_SUCCESS,
  REGISTER_USER_ERROR,
  SEND_OTP_USER,
  SEND_OTP_USER_ERROR,
  SEND_OTP_USER_SUCCESS,
  VERIFY_EMAIL_USER,
  VERIFY_EMAIL_USER_ERROR,
  VERIFY_EMAIL_USER_SUCCESS,
  LOGOUT_USER,
  FORGOT_PASSWORD,
  FORGOT_PASSWORD_SUCCESS,
  FORGOT_PASSWORD_ERROR,
  RESET_PASSWORD,
  RESET_PASSWORD_SUCCESS,
  RESET_PASSWORD_ERROR,
  UPDATE_USER,
  UPDATE_USER_SUCCESS,
  UPDATE_USER_ERROR,
  CHANGE_PASSWORD,
  CHANGE_PASSWORD_SUCCESS,
  CHANGE_PASSWORD_ERROR,
  DEPOSIT_HISTORY,
  DEPOSIT_HISTORY_SUCCESS,
  DEPOSIT_HISTORY_ERROR
} from '../contants';

const INIT_STATE = {
  currentUser: isAuthGuardActive ? currentUser : getCurrentUser(),
  currentUserProfile: isAuthGuardActive ? currentUser : getCurrentUserProfile(),
  forgotUserMail: '',
  newPassword: '',
  resetPasswordCode: '',
  loading: false,
  error: '',
  success: '',
  otpSend: false,
  verifyEmail: false,
  twoFactor: false,
  otpSendForgot: false,
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {

    case LOGIN_USER:
      return { ...state, loading: true, error: '' };

    case LOGIN_USER_SUCCESS:
      return {
        ...state,
        loading: false,
        currentUser: action.payload,
        success: action.payload.message,
      };
    case LOGIN_USER_PROFILE:
      return {
        ...state,
        loading: false,
        currentUserProfile: action.payload,
        success: action.payload.message,
      };
    case LOGIN_USER_ERROR:
      return {
        ...state,
        loading: false,
        currentUser: null,
        error: action.payload.message,
        otpSend: action.payload.otp_send,
        twoFactor: action.payload.two_factor
      };
    case SEND_OTP_USER:
      return { ...state, loading: true, error: '' };
    case SEND_OTP_USER_SUCCESS:
      return {
        ...state,
        loading: false,
        currentUser: null,
        success: action.payload.message,
        otpSendForgot: action.payload.otp_send_forgot
      };
    case SEND_OTP_USER_ERROR:
      return {
        ...state,
        loading: false,
        currentUser: null,
        error: action.payload.message
      };

    case VERIFY_EMAIL_USER:
      return { ...state, loading: true, error: '' };
    case VERIFY_EMAIL_USER_SUCCESS:
      return {
        ...state,
        loading: false,
        currentUser: null,
        success: action.payload.message,
      };
    case VERIFY_EMAIL_USER_ERROR:
      return {
        ...state,
        loading: false,
        currentUser: null,
        error: action.payload.message
      };
    case FORGOT_PASSWORD:
      return { ...state, loading: true, error: '' };
    case FORGOT_PASSWORD_SUCCESS:
      return {
        ...state,
        loading: false,
        forgotUserMail: action.payload,
        success: action.payload.message,
      };
    case FORGOT_PASSWORD_ERROR:
      return {
        ...state,
        loading: false,
        forgotUserMail: '',
        error: action.payload.message,
      };
    case RESET_PASSWORD:
      return { ...state, loading: true, error: '' };
    case RESET_PASSWORD_SUCCESS:
      return {
        ...state,
        loading: false,
        newPassword: action.payload,
        resetPasswordCode: '',
        error: action.payload.message,
      };
    case RESET_PASSWORD_ERROR:
      return {
        ...state,
        loading: false,
        newPassword: '',
        resetPasswordCode: '',
        error: action.payload.message,
      };
    case REGISTER_USER:
      return { ...state, loading: true, error: '' };
    case REGISTER_USER_SUCCESS:
      return {
        ...state,
        loading: false,
        currentUser: null,
        success: action.payload.message,
      };
    case REGISTER_USER_ERROR:
      return {
        ...state,
        loading: false,
        currentUser: null,
        error: action.payload.message,
      };

    case CHANGE_PASSWORD:
      return { ...state, loading: true, error: '' };

    case CHANGE_PASSWORD_SUCCESS:
      return {
        ...state,
        loading: false,
        newPassword: action.payload,
        resetPasswordCode: '',
        error: action.payload.message,
      };

    case CHANGE_PASSWORD_ERROR:
      return {
        ...state,
        loading: false,
        newPassword: '',
        resetPasswordCode: '',
        error: action.payload.message,
      };

    case LOGOUT_USER:
      return { ...state, currentUser: null, error: '' };
    default:
      return { ...state };

    case UPDATE_USER:
      return {
        ...state,
        loading: true,
        error: ''
      };

    case UPDATE_USER_SUCCESS:
      return {
        ...state,
        loading: false,
        error: action.payload.message,
      };

    case UPDATE_USER_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload.message,
      };


    case DEPOSIT_HISTORY:
      return {
        ...state,
        loading: true,
        error: ''
      };

    case DEPOSIT_HISTORY_SUCCESS:
      return {
        ...state,
        loading: false,
        error: action.payload.message,
      };

    case DEPOSIT_HISTORY_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload.message,
      };
  }
};
