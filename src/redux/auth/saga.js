import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import Cookie from 'js-cookie';

import { toast } from 'react-toastify';
import { auth } from 'helpers/Firebase';
import { adminRoot } from 'constants/defaultValues';
import { setCurrentUser, setCurrentUserProfile } from 'helpers/Utils';
import {
  SigninApi,
  SignupApi,
  RequestOtpApi,
  UserDetailsApi,
  VerifyEmailApi,
  ForgotPasswordApi,
  LogoutApi,
  ChangePasswordApi,
  UpdateProfileApi
} from 'service/user';


import {
  DepositHistoryApi,
  // GetTickerApi,
  // BinanceListAddressApi,
} from 'service/binance_wallet';

import {
  LOGIN_USER,
  SEND_OTP_USER,
  VERIFY_EMAIL_USER,
  REGISTER_USER,
  LOGOUT_USER,
  FORGOT_PASSWORD,
  RESET_PASSWORD,
  UPDATE_USER,
  CHANGE_PASSWORD,
  DEPOSIT_HISTORY,
} from '../contants';

import {
  loginUserSuccess,
  loginUserError,
  loginUserProfile,
  sendOtpUserSuccess,
  sendOtpUserError,
  verifyEmailUserSuccess,
  verifyEmailUserError,
  registerUserSuccess,
  registerUserError,
  forgotPasswordSuccess,
  forgotPasswordError,
  resetPasswordSuccess,
  resetPasswordError,
  changePasswordSuccess,
  changePasswordError,
  updateUserError,
  updateUserSuccess,
  depositHistorySuccess,
  depositHistoryError,
} from './actions';


export function* watchLoginUser() {
  // eslint-disable-next-line no-use-before-define
  yield takeEvery(LOGIN_USER, loginWithEmailPassword);
}

const loginWithEmailPasswordAsync = async (payload) =>
  // eslint-disable-next-line no-return-await
  await SigninApi(payload)
    .then((user) => user)
    .catch((error) => error);

const UserDetailsApiAsync = async (payload) =>
  // eslint-disable-next-line no-return-await
  await UserDetailsApi(payload)
    .then((user) => user)
    .catch((error) => error);

function* loginWithEmailPassword({ payload }) {
  const { history } = payload;
  try {
    const loginUser = yield call(loginWithEmailPasswordAsync, payload.user);
    if (loginUser.status === true) {
      Cookie.set('token', loginUser.access_token);
      localStorage.setItem('token', loginUser.access_token);
      const userDetails = yield call(UserDetailsApiAsync, {
        token: loginUser.access_token,
      });
      if (userDetails.status === true) {
        const item = userDetails.data;
        setCurrentUser(item);
        yield put(loginUserSuccess(item));
        setCurrentUserProfile(item.profile);
        yield put(loginUserProfile(item.profile));
        toast.success('Login Successfull!!');
        history.push(adminRoot);
      }
    } else if (loginUser.otp_send === true) {
      yield put(loginUserError(loginUser.message, loginUser.otp_send));
      toast.error(loginUser.message);
      history.push('/user/send-otp');
    } else if (loginUser.two_factor === true) {
      yield put(loginUserError(loginUser.message, false, loginUser.two_factor));
      toast.error(loginUser.message);
    } else {
      let Error = 'Login error!!';
      if (loginUser.errors?.username) {
        Error = loginUser.errors.username;
        yield put(loginUserError(Error[0]));
        toast.error(Error[0]);
      } else if (loginUser.errors?.password) {
        Error = loginUser.errors.password;
        yield put(loginUserError(Error[0]));
        toast.error(Error[0]);
      } else if (loginUser.errors?.two_factor_code) {
        Error = loginUser.errors.two_factor_code;
        yield put(loginUserError(Error[0]));
        toast.error(Error[0]);
      } else {
        yield put(loginUserError(loginUser.message));
        toast.error(loginUser.message);
      }
    }
  } catch (error) {
    toast.error(error);
    yield put(loginUserError(error));
  }
}


export function* watchSendOtpUser() {
  // eslint-disable-next-line no-use-before-define
  yield takeEvery(SEND_OTP_USER, sendOtpWithUsername);
}

const sendOtpWithUsernameAsync = async (payload) =>
  // eslint-disable-next-line no-return-await
  await RequestOtpApi(payload)
    .then((user) => user)
    .catch((error) => error);

function* sendOtpWithUsername({ payload }) {
  const { history } = payload;
  try {
    const sendOtpUser = yield call(
      sendOtpWithUsernameAsync,
      payload.user
    );

    if (sendOtpUser.status === true) {
      toast.success(sendOtpUser.message);
      if (payload.user.is_otp_send_forgot) {
        yield put(sendOtpUserSuccess(sendOtpUser.message, true));
      } else {
        yield put(sendOtpUserSuccess(sendOtpUser.message));
        history.push('/user/verify-email');
      }
    } else {
      let Error = 'Send otp error!!'
      if (sendOtpUser.errors?.username) {
        Error = sendOtpUser.errors.username;
        yield put(sendOtpUserError(Error[0]));
        toast.error(Error[0]);
      } else if (sendOtpUser.errors?.email) {
        Error = sendOtpUser.errors.email;
        yield put(sendOtpUserError(Error[0]));
        toast.error(Error[0]);
      } else {
        yield put(sendOtpUserError(Error));
        toast.error(Error);
      }

    }
  } catch (error) {
    yield put(registerUserError(error));
    toast.error(error);
  }
}

export function* watchVerifyEmailUser() {
  // eslint-disable-next-line no-use-before-define
  yield takeEvery(VERIFY_EMAIL_USER, verifyEmailWithOtp);
}

const verifyEmailWithOtpAsync = async (payload) =>
  // eslint-disable-next-line no-return-await
  await VerifyEmailApi(payload)
    .then((user) => user)
    .catch((error) => error);

function* verifyEmailWithOtp({ payload }) {
  const { history } = payload;
  try {
    const verifyEmailUser = yield call(
      verifyEmailWithOtpAsync,
      payload.user
    );

    if (verifyEmailUser.status === true) {
      yield put(verifyEmailUserSuccess(verifyEmailUser.message, true));
      toast.success(verifyEmailUser.message);
      history.push('/user/login')
    } else {
      let Error = 'Verify email error!!'
      if (verifyEmailUser.errors?.username) {
        Error = verifyEmailUser.errors.username;
        yield put(verifyEmailUserError(Error[0]));
        toast.error(Error[0]);
      } else if (verifyEmailUser.errors?.otp) {
        Error = verifyEmailUser.errors.otp;
        yield put(verifyEmailUserError(Error[0]));
        toast.error(Error[0]);
      } else {
        yield put(verifyEmailUserError(Error));
        toast.error(Error[0]);
      }
    }
  } catch (error) {
    yield put(registerUserError(error));
    toast.error(error);
  }
}

export function* watchRegisterUser() {
  // eslint-disable-next-line no-use-before-define
  yield takeEvery(REGISTER_USER, registerWithEmailPassword);
}

const registerWithEmailPasswordAsync = async (payload) =>
  // eslint-disable-next-line no-return-await
  await SignupApi(payload)
    .then((user) => user)
    .catch((error) => error);

function* registerWithEmailPassword({ payload }) {
  const { history } = payload;
  try {
    const registerUser = yield call(
      registerWithEmailPasswordAsync,
      payload.user
    );

    if (registerUser.status === true) {
      yield put(registerUserSuccess(registerUser.message));
      toast.success(registerUser.message);
      history.push(adminRoot);
    } else {
      let Error = 'Register error!!';
      if (registerUser.errors?.name) {
        Error = registerUser.errors.name;
        yield put(registerUserError(Error[0]));
        toast.error(Error[0]);
      } else if (registerUser.errors?.username) {
        Error = registerUser.errors.username;
        yield put(registerUserError(Error[0]));
        toast.error(Error[0]);
      } else if (registerUser.errors?.email) {
        Error = registerUser.errors.email;
        yield put(registerUserError(Error[0]));
        toast.error(Error[0]);
      } else if (registerUser.errors?.password) {
        Error = registerUser.errors.password;
        yield put(registerUserError(Error[0]));
        toast.error(Error[0]);
      } else {
        yield put(registerUserError(Error));
        toast.error(Error);
      }
    }
  } catch (error) {
    yield put(registerUserError(error));
    toast.error(error);
  }
}

export function* watchLogoutUser() {
  // eslint-disable-next-line no-use-before-define
  yield takeEvery(LOGOUT_USER, logout);
}

const logoutAsync = async (history) => {
  await LogoutApi({})
    .then((user) => user)
    .catch((error) => error);
  toast.success('Logout Successful!!');
  history.push(adminRoot);
  // if(logoutUser.status === true){
  //   toast.success(logoutUser.message);
  //   history.push(adminRoot);
  // }else{
  //   toast.error(logoutUser.message);
  // }
};

function* logout({ payload }) {
  const { history } = payload;
  setCurrentUser();
  setCurrentUserProfile();
  yield call(logoutAsync, history);
}

export function* watchForgotPassword() {
  // eslint-disable-next-line no-use-before-define
  yield takeEvery(FORGOT_PASSWORD, forgotPassword);
}

const forgotPasswordAsync = async (email) => {
  // eslint-disable-next-line no-return-await
  return await auth
    .sendPasswordResetEmail(email)
    .then((user) => user)
    .catch((error) => error);
};

function* forgotPassword({ payload }) {
  const { email } = payload.forgotUserMail;
  try {
    const forgotPasswordStatus = yield call(forgotPasswordAsync, email);
    if (!forgotPasswordStatus) {
      yield put(forgotPasswordSuccess('success'));
    } else {
      yield put(forgotPasswordError(forgotPasswordStatus.message));
    }
  } catch (error) {
    yield put(forgotPasswordError(error));
  }
}

export function* watchResetPassword() {
  // eslint-disable-next-line no-use-before-define
  yield takeEvery(RESET_PASSWORD, resetPassword);
}

const resetPasswordAsync = async (payload) => {
  // eslint-disable-next-line no-return-await
  return await ForgotPasswordApi(payload)
    .then((user) => user)
    .catch((error) => error);
};

function* resetPassword({ payload }) {
  const { history } = payload;

  try {
    const resetPasswordUser = yield call(
      resetPasswordAsync,
      payload.user
    );
    console.log(resetPasswordUser);
    if (resetPasswordUser.status === true) {
      yield put(resetPasswordSuccess(resetPasswordUser.message));
      toast.success(resetPasswordUser.message);
      history.push('/user/login');
    } else {
      let Error = 'Reset Password error!!';
      if (resetPasswordUser.errors?.username) {
        Error = resetPasswordUser.errors.username;
        yield put(resetPasswordError(Error[0]));
        toast.error(Error[0]);
      } else if (resetPasswordUser.errors?.newpassword) {
        Error = resetPasswordUser.errors.newpassword;
        yield put(resetPasswordError(Error[0]));
        toast.error(Error[0]);
      } else if (resetPasswordUser.errors?.otp) {
        Error = resetPasswordUser.errors.otp;
        yield put(resetPasswordError(Error[0]));
        toast.error(Error[0]);
      } else if (resetPasswordUser.message) {
        Error = resetPasswordUser.message;
        yield put(resetPasswordError(Error));
        toast.error(Error);
      } else {
        yield put(resetPasswordError(Error));
        toast.error(Error);
      }
    }
  } catch (error) {
    console.log(error);
    yield put(resetPasswordError(error));
    toast.error(error);
  }
}


export function* watchUpdateUser() {
  // eslint-disable-next-line no-use-before-define
  yield takeEvery(UPDATE_USER, update);
}

const updateUserAsync = async (payload) => {
  // eslint-disable-next-line no-return-await
  return await UpdateProfileApi(payload)
    .then((user) => user)
    .catch((error) => error);
};

function* update({ payload }) {
  const { history, user } = payload;

  try {
    const updateUser = yield call(
      updateUserAsync,
      user
    );
    console.log(updateUser);
    if (updateUser.status === true) {
      yield put(updateUserSuccess(updateUser.message));
      toast.success(updateUser.message);
      history.push('/user/login');
    } else {
      let Error = 'Profile error!!';

      if (updateUser.errors?.address_line1) {
        Error = updateUser.errors.address_line1;
        yield put(updateUserError(Error[0]));
        toast.error(Error[0]);

      } else if (updateUser.errors?.address_line_2) {
        Error = updateUser.errors.address_line_2;
        yield put(updateUserError(Error[0]));
        toast.error(Error[0]);

      } else if (updateUser.errors?.city) {
        Error = updateUser.errors.city;
        yield put(updateUserError(Error[0]));
        toast.error(Error[0]);

      } else if (updateUser.errors?.country) {
        Error = updateUser.errors.country;
        yield put(updateUserError(Error[0]));
        toast.error(Error[0]);

      } else if (updateUser.errors?.dob) {
        Error = updateUser.errors.dob;
        yield put(updateUserError(Error[0]));
        toast.error(Error[0]);

      } else if (updateUser.errors?.email) {
        Error = updateUser.errors.email;
        yield put(updateUserError(Error[0]));
        toast.error(Error[0]);

      } else if (updateUser.errors?.gender) {
        Error = updateUser.errors.gender;
        yield put(updateUserError(Error[0]));
        toast.error(Error[0]);

      } else if (updateUser.errors?.landmark) {
        Error = updateUser.errors.landmark;
        yield put(updateUserError(Error[0]));
        toast.error(Error[0]);

      } else if (updateUser.errors?.name) {
        Error = updateUser.errors.name;
        yield put(updateUserError(Error[0]));
        toast.error(Error[0]);

      } else if (updateUser.errors?.phone) {
        Error = updateUser.errors.phone;
        yield put(updateUserError(Error[0]));
        toast.error(Error[0]);

      } else if (updateUser.errors?.pincode) {
        Error = updateUser.errors.pincode;
        yield put(updateUserError(Error[0]));
        toast.error(Error[0]);

      } else if (updateUser.errors?.state) {
        Error = updateUser.errors.state;
        yield put(updateUserError(Error[0]));
        toast.error(Error[0]);

      } else if (updateUser.errors?.tax_id) {
        Error = updateUser.errors.tax_id;
        yield put(updateUserError(Error[0]));
        toast.error(Error[0]);

      } else if (updateUser.message) {
        Error = updateUser.message;
        yield put(updateUserError(Error));
        toast.error(Error);

      } else {
        yield put(updateUserError(Error));
        toast.error(Error);
      }
    }
  } catch (error) {
    console.log(error);
    yield put(updateUserError(error));
    toast.error(error);
  }
}


export function* watchChangePassword() {
  // eslint-disable-next-line no-use-before-define
  yield takeEvery(CHANGE_PASSWORD, changePassword);
}

const changePasswordAsync = async (payload) => {
  // eslint-disable-next-line no-return-await
  return await ChangePasswordApi(payload)
    .then((user) => user)
    .catch((error) => error);
};

function* changePassword({ payload }) {
  const { history } = payload;

  try {
    const changePasswordUser = yield call(
      changePasswordAsync,
      payload.user
    );
    console.log(changePasswordUser);
    if (changePasswordUser.status === true) {
      yield put(changePasswordSuccess(changePasswordUser.message));
      toast.success(changePasswordUser.message);
      history.push('/user/login');
    } else {
      let Error = 'Reset Password error!!';

      if (changePasswordUser.errors?.username) {
        Error = changePasswordUser.errors.username;
        yield put(changePasswordError(Error[0]));
        toast.error(Error[0]);

      } else if (changePasswordUser.errors?.newpassword) {
        Error = changePasswordUser.errors.newpassword;
        yield put(changePasswordError(Error[0]));
        toast.error(Error[0]);
      } else if (changePasswordUser.errors?.otp) {
        Error = changePasswordUser.errors.otp;
        yield put(changePasswordError(Error[0]));
        toast.error(Error[0]);
      } else if (changePasswordUser.message) {
        Error = changePasswordUser.message;
        yield put(changePasswordError(Error));
        toast.error(Error);
      } else {
        yield put(changePasswordError(Error));
        toast.error(Error);
      }
    }
  } catch (error) {
    console.log(error);
    yield put(changePasswordError(error));
    toast.error(error);
  }
}



export function* watchDepositHistory() {
  // eslint-disable-next-line no-use-before-define
  yield takeEvery(DEPOSIT_HISTORY, depositHistory);
}

const depositHistoryAsync = async (payload) => {
  // eslint-disable-next-line no-return-await
  return await DepositHistoryApi(payload)
    .then((user) => user)
    .catch((error) => error);
};

function* depositHistory({ payload }) {

  try {
    const depositHistoryUser = yield call(
      depositHistoryAsync,
      payload.user
    );

    console.log(depositHistoryUser);
    if (depositHistoryUser.status === true) {
      yield put(depositHistorySuccess(depositHistoryUser.message));
    // } 
      // else {
      // let Error = 'Reset Password error!!';

      // if (depositHistoryUser.errors?.username) {
      //   Error = depositHistoryUser.errors.username;
      //   yield put(depositHistoryError(Error[0]));
      //   toast.error(Error[0]);

      // } else if (depositHistoryUser.errors?.newpassword) {
      //   Error = depositHistoryUser.errors.newpassword;
      //   yield put(depositHistoryError(Error[0]));
      //   toast.error(Error[0]);

      // } else if (depositHistoryUser.errors?.otp) {
      //   Error = depositHistoryUser.errors.otp;
      //   yield put(depositHistoryError(Error[0]));
      //   toast.error(Error[0]);

      // } else if (depositHistoryUser.message) {
      //   Error = depositHistoryUser.message;
      //   yield put(depositHistoryError(Error));
      //   toast.error(Error);

      // } else {
      //   yield put(depositHistoryError(Error));
      //   toast.error(Error);
      // }
    }
  } catch (error) {
    console.log(error);
    yield put(depositHistoryError(error));
  }
}

export default function* rootSaga() {
  yield all([
    fork(watchLoginUser),
    fork(watchSendOtpUser),
    fork(watchVerifyEmailUser),
    fork(watchLogoutUser),
    fork(watchRegisterUser),
    fork(watchForgotPassword),
    fork(watchResetPassword),
    fork(watchUpdateUser),
    fork(watchChangePassword),
    fork(watchDepositHistory),

  ]);
}
