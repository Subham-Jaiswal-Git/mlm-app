import {
  LOGIN_USER,
  LOGIN_USER_SUCCESS,
  LOGOUT_USER,
  LOGIN_USER_PROFILE,
  REGISTER_USER,
  REGISTER_USER_SUCCESS,
  LOGIN_USER_ERROR,
  REGISTER_USER_ERROR,
  SEND_OTP_USER,
  SEND_OTP_USER_ERROR,
  SEND_OTP_USER_SUCCESS,
  VERIFY_EMAIL_USER,
  VERIFY_EMAIL_USER_ERROR,
  VERIFY_EMAIL_USER_SUCCESS,
  FORGOT_PASSWORD,
  FORGOT_PASSWORD_SUCCESS,
  FORGOT_PASSWORD_ERROR,
  RESET_PASSWORD,
  RESET_PASSWORD_SUCCESS,
  RESET_PASSWORD_ERROR,
 
  CHANGE_PASSWORD,
  CHANGE_PASSWORD_SUCCESS,
  CHANGE_PASSWORD_ERROR,

  UPDATE_USER,
  UPDATE_USER_SUCCESS,
  UPDATE_USER_ERROR,

  DEPOSIT_HISTORY,
  DEPOSIT_HISTORY_SUCCESS,
  DEPOSIT_HISTORY_ERROR
} from '../contants';

export const loginUser = (user, history) => ({
  type: LOGIN_USER,
  payload: { user, history },
});
export const loginUserSuccess = (user) => ({
  type: LOGIN_USER_SUCCESS,
  payload: user,
});
export const loginUserError = (message, otp_send=false, two_factor=false) => ({
  type: LOGIN_USER_ERROR,
  payload: { message, otp_send, two_factor },
});
export const loginUserProfile = (user) => ({
  type: LOGIN_USER_PROFILE,
  payload: user,
});

export const sendOtpUser = (user, history) => ({
  type: SEND_OTP_USER,
  payload: { user, history },
});
export const sendOtpUserSuccess = (message, otp_send_forgot=false) => ({
  type:  SEND_OTP_USER_SUCCESS,
  payload: { message, otp_send_forgot },
});
export const sendOtpUserError = (message) => ({
  type:  SEND_OTP_USER_ERROR,
  payload: { message },
});

export const verifyEmailUser = (user, history) => ({
  type: VERIFY_EMAIL_USER,
  payload: { user, history },
});
export const verifyEmailUserSuccess = (message) => ({
  type:  VERIFY_EMAIL_USER_SUCCESS,
  payload: { message },
});
export const verifyEmailUserError = (message) => ({
  type:  VERIFY_EMAIL_USER_ERROR,
  payload: { message },
});

export const forgotPassword = (forgotUserMail, history) => ({
  type: FORGOT_PASSWORD,
  payload: { forgotUserMail, history },
});
export const forgotPasswordSuccess = (forgotUserMail) => ({
  type: FORGOT_PASSWORD_SUCCESS,
  payload: forgotUserMail,
});
export const forgotPasswordError = (message) => ({
  type: FORGOT_PASSWORD_ERROR,
  payload: { message },
});

export const resetPassword = (user, history) => ({
  type: RESET_PASSWORD,
  payload: { user, history },
});
export const resetPasswordSuccess = (newPassword) => ({
  type: RESET_PASSWORD_SUCCESS,
  payload: newPassword,
});
export const resetPasswordError = (message) => ({
  type: RESET_PASSWORD_ERROR,
  payload: { message },
});

export const registerUser = (user, history) => ({
  type: REGISTER_USER,
  payload: { user, history },
});
export const registerUserSuccess = (message) => ({
  type: REGISTER_USER_SUCCESS,
  payload: { message },
});
export const registerUserError = (message) => ({
  type: REGISTER_USER_ERROR,
  payload: { message },
});

export const logoutUser = (history) => ({
  type: LOGOUT_USER,
  payload: { history },
});

export const updateUser = (user,history) => ({
  type: UPDATE_USER,
  payload: { user ,history},
});


export const updateUserSuccess = (message) => ({
  type: UPDATE_USER_SUCCESS,
  payload: { message },
});
export const updateUserError = (message) => ({
  type: UPDATE_USER_ERROR,
  payload: { message },
});

export const changePassword = (user, history) => ({
  type: CHANGE_PASSWORD,
  payload: { user,history },
});

export const changePasswordSuccess = (newPassword) => ({
  type: CHANGE_PASSWORD_SUCCESS,
  payload: newPassword,
});
export const changePasswordError = (message) => ({
  type: CHANGE_PASSWORD_ERROR,
  payload: { message },
});

// export const loginUserSuccess = (user) => ({
//   type: LOGIN_USER_SUCCESS,
//   payload: user,
// });

export const depositHistory = (user) => ({
  type: DEPOSIT_HISTORY,
  payload: { user },
});

export const depositHistorySuccess = (user) => ({
  type: DEPOSIT_HISTORY_SUCCESS,
  payload: {user},
});
export const depositHistoryError = (message) => ({
  type: DEPOSIT_HISTORY_ERROR,
  payload: { message },
});