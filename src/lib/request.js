import axios from 'axios';
import Cookie from 'js-cookie';
import { apiUrl } from '../config';

const request = axios.create({
  baseURL: apiUrl,
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  },
});

/* eslint-disable no-param-reassign */
request.interceptors.request.use((config) => {
  const token = Cookie.get('token');
  const lang = global.window && window.location.href.split('/')[3];

  /* eslint no-else-return: 'error' */
  
  if (token) {
    config.headers.Authorization = `Bearer ${token}`;
  }

  /* eslint no-unneeded-ternary: 'error' */

  config.headers.lang = lang ? lang : 'en';
  config.headers.userapisecret = process.env.PUBLIC_SECRET_KEY;
  return config;
});
request.interceptors.response.use((response) => {
  return response;
});


export function apiRequest(base, query=null) {
  /* eslint-disable camelcase */
  let return_data;
  if (query === null) {
    return_data = request(base);
  } else {
    return_data = axios.get(base + query);
  }
  return return_data;
}
export default request;
