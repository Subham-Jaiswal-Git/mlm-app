export const apiUrl = 'https://mlm.cryptrum.com/api';
export const baseUrl = 'https://mlm.cryptrum.com';
export const avatarUrl = 'https://mlm.cryptrum.com/avatars';
export const imageUrl = 'https://mlm.cryptrum.com/images';