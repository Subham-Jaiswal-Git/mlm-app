/* 
MLM Language Texts
Table of Contents
01.General
02.User Login, Logout, Register
03.Menu
04.Error Page
*/

module.exports = {
  /* 01.General */
  'general.copyright': 'MLM React © Todos los derechos reservados.',

  'unauthorized.title': 'Unauthorized Access Attempt',
  'unauthorized.detail':
    'You are not authorized to view the page you are trying to access.',

  /* 02.Inicio de sesión de usuario, cierre de sesión, registro */
  'user.login-title': 'Iniciar sesión',
  'user.register': 'Registro',
  'user.forgot-password': 'Se te olvidó tu contraseña',
  'user.email': 'E-mail',
  'user.password': 'Contraseña',
  'user.forgot-password-question': '¿Contraseña olvidada?',
  'user.fullname': 'Nombre completo',
  'user.login-button': 'INICIAR SESIÓN',
  'user.register-button': 'REGISTRO',
  'user.reset-password-button': 'REINICIAR',
  'user.buy': 'COMPRAR',
  'user.username': 'Nombre de Usuario',
  'user.create-account-question': 'Already have an account? ',
  'user.send-otp': 'Send OTP',
  'user.send-otp-button': 'SEND OTP',
  'user.verify-email': 'Verify Email',
  'user.verify-email-button': 'VERIFY EMAIL',
  'user.otp': 'OTP',
  'user.two-factor-code': 'Two Factor Code',
  'user.new-password': 'New Password',
  'user.profile':'save',

  /* 03.Menú */
  'menu.home': 'Inicio',
  'menu.app': 'Inicio',
  'menu.dashboards': 'Tableros',
  'menu.dashboard': 'Tablero',
  'menu.gogo': 'Gogo',
  'menu.start': 'Comienzo',
  'menu.second-menu': 'Segundo menú',
  'menu.second': 'Segundo',

  'menu.wallet': 'Wallet',

  'menu.tickets-list': 'Tickets List',
  'menu.ticketsList': 'Tickets List',
  'menu.depositHistory': 'Deposit History',
  'menu.deposit-history': 'Deposit History',
  'menu.binanceList': 'Binance List',
  'menu.binance-list': 'Binance List',

  'menu.wallet-binanceList': 'Binance List',
  'menu.wallet-ticketsList':'Tickets List',
  'menu.wallet-depositHistory': 'Deposit History',

  'menu.ui': 'IU',
  'menu.charts': 'Gráficos',
  'menu.chat': 'Chatea',
  'menu.survey': 'Encuesta',
  'menu.todo': 'Notas',
  'menu.search': 'Búsqueda',
  'menu.docs': 'Docs',
  'menu.blank-page': 'Blank Page',
  'menu.profile': 'Profile',
  'menu.change-password':'Change Password',
  /* 04.Error  */
  'layouts.error-title': 'Vaya, parece que ha ocurrido un error!',
  'layouts.error-code': 'Código de error',
  'layouts.go-back-home': 'REGRESAR A INICIO',
  
  'profile.Tax_ID':'Tax ID',
  'profile.Mobile':'Mobile',
  'profile.Gender':'Gender',
  'profile.fullname':'Full Name',
  'profile.Address_Line_1':'Address Line 1',
  'profile.Address_Line_2':'Address Line 2',
  'profile.Landmark':'Landmark',
  'profile.State':'State',
  'profile.Pincode':'Pincode',
  'profile.City':'City',
  'profile.DOB':'DOB',
  'profile.Country':'Country',
     /* 05.Change Password Page */
     'password.currentpassword':"Current Password",
     'password.newpassword':"New Password"
};
