/* MLM Language Texts

Table of Contents

01.General
02.User Login, Logout, Register
03.Menu
04.Error Page
*/

module.exports = {
  /* 01.General */
  'general.copyright': 'MLM React © 2023 All Rights Reserved.',

  'unauthorized.title': 'Unauthorized Access Attempt',
  'unauthorized.detail':
    'You are not authorized to view the page you are trying to access.',

  /* 02.User Login, Logout, Register */
  'user.login-title': 'Login',
  'user.register': 'Register',
  'user.forgot-password': 'Forgot Password',
  'user.email': 'E-mail',
  'user.password': 'Password',
  'user.forgot-password-question': 'Forget password?',
  'user.fullname': 'Full Name',
  'user.login-button': 'LOGIN',
  'user.register-button': 'REGISTER',
  'user.reset-password-button': 'RESET',
  'user.buy': 'BUY',
  'user.username': 'Username',
  'user.create-account-question': 'Already have an account? ',
  'user.new-user-question': `Don't have an account? `,
  'user.send-otp': 'Send OTP',
  'user.send-otp-button': 'SEND OTP',
  'user.verify-email': 'Verify Email',
  'user.verify-email-button': 'VERIFY EMAIL',
  'user.otp': 'OTP',
  'user.two-factor-code': 'Two Factor Code',
  'user.new-password': 'New Password',
  'user.profile':'save',

  /* 03.Menu */
  'menu.home': 'Home',
  'menu.app': 'Home',
  'menu.dashboards': 'Dashboards',
  'menu.dashboard': 'Dashboard',
  'menu.gogo': 'Gogo',
  'menu.start': 'Start',
  'menu.second-menu': 'Second Menu',

  'menu.wallet': 'Wallet',

  'menu.tickets-list': 'Tickets List',
  'menu.ticketsList': 'Tickets List',
  'menu.depositHistory': 'Deposit History',
  'menu.deposit-history': 'Deposit History',
  'menu.binanceList': 'Binance List',
  'menu.binance-list': 'Binance List',

  'menu.wallet-binanceList': 'Binance List',
  'menu.wallet-ticketsList':'Tickets List',
  'menu.wallet-depositHistory': 'Deposit History',
  
  'menu.second': 'Second',
  'menu.ui': 'UI',
  'menu.charts': 'Charts',
  'menu.chat': 'Chat',
  'menu.survey': 'Survey',
  'menu.todo': 'Todo',
  'menu.search': 'Search',
  'menu.docs': 'Docs',
  'menu.blank-page': 'Blank Page',
  'menu.profile': 'Profile',
  'menu.change-password':'Change Password',
  /* 04.Error Page */
  'pages.error-title': 'Ooops... looks like an error occurred!',
  'pages.error-code': 'Error code',
  'pages.go-back-home': 'GO BACK HOME',

  'profile.Tax_ID':'Tax ID',
  'profile.Mobile':'Mobile',
  'profile.Gender':'Gender',
  'profile.fullname':'Full Name',
  'profile.Address_Line_1':'Address Line 1',
  'profile.Address_Line_2':'Address Line 2',
  'profile.Landmark':'Landmark',
  'profile.State':'State',
  'profile.Pincode':'Pincode',
  'profile.City':'City',
  'profile.DOB':'DOB',
  'profile.Country':'Country',
   /* 05.Change Password Page */
   'password.currentpassword':"Current Password",
   'password.newpassword':"New Password",
   /* 05.kyc  Page */
   'menu.kyc':"KYC",
   'menu.kyc-upload':"KYC Upload",
   'menu.kyc-list':'KYC List',
};
