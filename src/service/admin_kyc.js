import request from '../lib/request';

export const KycVerifyApi = async (credentials) => {
  const { data } = await request.post('/kyc-verify', credentials);
  return data;
};

export const KycPendingApi = async (credentials) => {
  const { data } = await request.post('/kyc-pending', credentials);
  return data;
};

export const KycRejectApi = async (credentials) => {
  const { data } = await request.post('/kyc-reject', credentials);
  return data;
};

export const KycDocVerifyApi = async (credentials) => {
  const { data } = await request.post('/kyc-doc-verify', credentials);
  return data;
};

export const KycDocRejectApi = async (credentials) => {
  const { data } = await request.post('/kyc-doc-reject', credentials);
  return data;
};

export const KycDocPendingApi = async (credentials) => {
  const { data } = await request.post('/kyc-doc-pending', credentials);
  return data;
};

export const KycListAllApi = async (credentials) => {
  const { data } = await request.post('/kyc-list-all', credentials);
  return data;
};
