import request from '../lib/request';

export const KycDocUploadApi = async (credentials) => {
  const { data } = await request.post('/kyc-doc-upload', credentials);
  return data;
};

export const KycListApi = async (credentials) => {
  const { data } = await request.post('/kyc-list', credentials);
  return data;
};
