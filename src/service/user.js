import request from '../lib/request';

export const SigninApi = async (credentials) => {
  const { data } = await request.post('/login', credentials);
  return data;
};

export const SignupApi = async (credentials) => {
  const { data } = await request.post('/register', credentials);
  return data;
};

export const RequestOtpApi = async (credentials) => {
  const { data } = await request.post('/requestotp', credentials);
  return data;
};

export const VerifyEmailApi = async (credentials) => {
  const { data } = await request.post('/verify-email', credentials);
  return data;
};

export const ForgotPasswordApi = async (credentials) => {
  const { data } = await request.post('/forget-password', credentials);
  return data;
};

export const ResetPasswordApi = async (credentials) => {
  const { data } = await request.post('/reset-password', credentials);
  return data;
};

export const ChangePasswordApi = async (credentials) => {
  const { data } = await request.post('/change-password', credentials);
  return data;
};

export const UserDetailsApi = async (credentials) => {
  const { data } = await request.post('/user', credentials);
  return data;
};

export const UserProfileApi = async (credentials) => {
  const { data } = await request.post('/profile', credentials);
  return data;
};

export const UpdateProfileApi = async (credentials) => {
  const { data } = await request.post('/update-profile', credentials);
  return data;
};

export const LogoutApi = async (credentials) => {
  const { data } = await request.post('/logout', credentials);
  return data;
};

