import request from '../lib/request';

export const BinanceListAddressApi = async (credentials) => {
  const { data } = await request.post('/binance-list-address', credentials);
  return data;
};

export const GetTickerApi = async (credentials) => {
  const { data } = await request.post('/get-ticker', credentials);
  return data;
};

export const DepositHistoryApi = async (credentials) => {
  const { data } = await request.post('/deposit-history', credentials);
  return data;
};
