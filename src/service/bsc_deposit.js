import request from '../lib/request';

export const BscCreateAddressApi = async (credentials) => {
  const { data } = await request.post('/bsc-create-address', credentials);
  return data;
};

export const SyncBnbTransactionsApi = async (credentials) => {
  const { data } = await request.post('/sync-bnb-transactions', credentials);
  return data;
};

export const SyncBnbTokenTransactionsApi = async (credentials) => {
  const { data } = await request.post('/sync-bnb-token-transactions', credentials);
  return data;
};

export const CheckBnbTransactionsHashApi = async (credentials) => {
  const { data } = await request.post('/check-bnb-transactions-hash', credentials);
  return data;
};

export const WithdrawBnbApi = async (credentials) => {
  const { data } = await request.post('/withdraw-bnb', credentials);
  return data;
};