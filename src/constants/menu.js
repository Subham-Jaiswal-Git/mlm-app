import { adminRoot } from './defaultValues';

const data = [
  {
    id: 'dashboard',
    icon: 'iconsminds-air-balloon-1',
    label: 'menu.dashboard',
    to: `${adminRoot}/dashboard`,    
  },
  {
    id: 'Wallet',
    icon: 'iconsminds-three-arrow-fork',
    label: 'menu.wallet',
    to: `${adminRoot}/wallet`,
    // roles: [UserRole.Admin, UserRole.Editor],
    subs: [
      {
        icon: 'simple-icon-paper-plane',
        label: 'menu.wallet-binanceList',
        to: `${adminRoot}/wallet/binance-list`,
      },
      {
        icon: 'simple-icon-paper-plane',
        label: 'menu.wallet-ticketsList',
        to: `${adminRoot}/wallet/tickets-list`,
      },
      {
        icon: 'simple-icon-paper-plane',
        label: 'menu.wallet-depositHistory',
        to: `${adminRoot}/wallet/deposit-history`,
      },
    ],
  },
  {
    id: 'kyc',
    icon: 'iconsminds-three-arrow-fork',
    label: 'menu.kyc',
    to: `${adminRoot}/kyc`,
    // roles: [UserRole.Admin, UserRole.Editor],
    subs: [
      {
        icon: 'simple-icon-paper-plane',
        label: 'menu.kyc-upload',
        to: `${adminRoot}/kyc/kyc-upload`,
      },
      {
        icon: 'simple-icon-paper-plane',
        label: 'menu.kyc-list',
        to: `${adminRoot}/kyc/kyc-list`,
      }
    ],
  },
  {
    id: 'secondmenu',
    icon: 'iconsminds-three-arrow-fork',
    label: 'menu.second-menu',
    to: `${adminRoot}/second-menu`,
    // roles: [UserRole.Admin, UserRole.Editor],
    subs: [
      {
        icon: 'simple-icon-paper-plane',
        label: 'menu.second',
        to: `${adminRoot}/second-menu/second`,
      },
    ],
  },
  {
    id: 'blankpage',
    icon: 'iconsminds-bucket',
    label: 'menu.blank-page',
    to: `${adminRoot}/blank-page`,
  }
  
  
];
export default data;
